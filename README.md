# Minimal Python Docker Setup

## Setup

I used `direnv` to set the following variables:

```
export IMAGENAME=jldowns/gameoflife
export VERSION=0.1
```

`build.sh` will build the image.

`dev.sh` will run an interactive shell inside the container.
