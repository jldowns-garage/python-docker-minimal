# Start with the official Python 3.12 image
FROM python:3.11-bullseye

# Install mamba
RUN wget -qO- https://micromamba.snakepit.net/api/micromamba/linux-64/latest | tar -xvj bin/micromamba --strip-components=1
RUN ./micromamba shell init -s bash -p ~/micromamba
ENV PATH=$HOME/micromamba/bin:$PATH

# Source the modified .bashrc
RUN echo "source ~/.bashrc" >> ~/.bash_profile
SHELL ["/bin/bash", "--login", "-c"]

# Copy the requirements.txt file into the container
COPY requirements.txt /app/requirements.txt

# Set the working directory
WORKDIR /app

# Install the required packages using mamba
RUN micromamba install -y -n base -f requirements.txt && micromamba clean --all --yes
